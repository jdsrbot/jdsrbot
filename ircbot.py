﻿#!/usr/bin/env python
# # -*- coding: utf-8 -*-
#
# Copyright 2018, Philippe Grégoire <pg@pgregoire.xyz>
#
# ==================================================================== #

import datetime
import http.client
import irc.buffer
import irc.bot
import json

irc.client.ServerConnection.buffer_class = irc.buffer.LenientDecodingLineBuffer

# -------------------------------------------------------------------- #

def log(l, s):
	try:
		loglevel = config.loglevel
	except:
		loglevel = 'info'

	if 'D' == l and 'debug' != loglevel:
		return

	print('[{0}] {1}'.format(l, s))

	
def logi(s):
	log('I', s)


def logd(s):
	log('D', s)

# -------------------------------------------------------------------- #

class JDSRBot(irc.bot.SingleServerIRCBot):
	def __init__(self, user, clientid, token, channel, mgr):
		self.clientid = clientid
		self.token    = token
		self.channel  = '#{0}'.format(channel)
		self.chanid   = self.get_channel_id(user)
		self.manager  = mgr

		logi('Connecting to Twitch IRC')
		s = 'irc.chat.twitch.tv'
		p = 6667
		irc.bot.SingleServerIRCBot.__init__(self, [(s,p,'oauth:'+token)], '15obot', user)


	def get_channel_id(self, chan=None):
		c = chan or self.channel
		u = '/helix/users?login=' + c
		h = {'Client-ID': self.clientid}
		s = http.client.HTTPSConnection('api.twitch.tv')
		s.request('GET', u, headers=h)
		r = s.getresponse()

		j = json.loads(r.read().decode('utf-8'))
		if 'error' in j:
			raise Exception(j['error'])

		return j['data'][0]['id']


	# on_events
	# https://github.com/jaraco/irc/blob/master/irc/events.py


	def on_welcome(self, c, e):
		logi('Joining channel {0}'.format(self.channel))
		c.join(self.channel)


	def on_join(self, c, e):
		logi('Joined channel {0}'.format(e.target))
		logi('Listening for commands')


	def on_pubmsg(self, c, e):
		logd('{0} wrote: {1}'.format(e.source.split('!')[0], e.arguments[0]))

		now = datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S+00:00")

		with open('chat.log', 'a') as fp:
			fp.write('[{0}] {1} wrote: {2}\n'.format(now, e.source.split('!')[0], e.arguments[0]))

		if '!' == e.arguments[0][0]:
			self.on_command(c, e)


	def get_sender(self, e):
		return e.source.split('!')[0]
		

	def is_owner(self, user):
		return '#'+user == self.channel


	def on_command(self, c, e):
		v = e.arguments[0][1:].split()

		if 'sr' == v[0]:
			s = ' '.join(v[1:])
			u = self.get_sender(e)
			logd('{0} requested: {1}'.format(u, s))
			f = self.manager.request(s, u)
			if f:
				# TODO artist
				c.privmsg(self.channel, '{0} has been added to the queue'.format(f))
			else:
				c.privmsg(self.channel, 'Error 404: "{0}" was not found on this server'.format(s))

		if 'clear' == v[0]:
			if self.is_owner(self.get_sender(e)):
				self.manager.clear()

		if 'queue' == v[0]:
			q = self.manager.queue()
			if 0 == len(q):
				m = 'nothing! Whut?'
			else:
				q = [_['name'] for _ in q]
				m = q[0] if 1 == len(q) else ', '.join(q[:-1]) + ((' and ' + q[-1]) if 1 < len(q) else '')
			c.privmsg(self.channel, 'The queue contains {0}'.format(m))

		if 'pop' == v[0]:
			if self.is_owner(self.get_sender(e)):
				self.manager.pop()

# ==================================================================== #
