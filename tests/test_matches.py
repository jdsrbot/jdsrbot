
from tests.helpers import *

# TODO sway, imya 505 [enc]
# TODO these boots
# TODO juju
def test_matches():
	tests = [
		('PoPiPo', 'popip'),
		('Rock n Roll', 'rock n roll'),
		('Mi Gente', 'mi gente'),
		('Cola Song', 'cola'),
		('99 Luftballons', 'luftballoons'),
		('99 Luftballons', 'luftballons'),
		('99 Luftballons', 'luftbalons'),
		('99 Luftballons', 'loftballoons'),
		('99 Luftballons', 'loftballoons'),
		('Baby One More Time', 'baby 1 more time'),
		('HandClap', 'handclap'),
		('Gimme! Gimme! Gimme! (A Man After Midnight)', 'gimme gimme'),
		('Apache (Jump On It)', 'apache'),
		('Am I Wrong', 'am i wrong'),
		('Big Girl (You Are Beautiful)', 'big girl'),
		('Big Girl (You Are Beautiful)', 'big girls'),
		('Day-O (The Banana Boat Song)', 'day-o'),
		('Day-O (The Banana Boat Song)', 'dayo'),
		('Danse (Pop Version)', 'danse'),
		('DADDY', 'daddy'),
		("Don't Worry Be Happy", 'dont worry be happy'),
		('Feel It Still', 'feel it still'),
		('Flashdance ... What a Feeling', 'flashdance'),
		('Get Ugly', 'get ugly'),
		('Istanbul (Not Constantinople)', 'istanbul'),
		('Istanbul (Not Constantinople)', 'Istanbul (Not Constantinople)'),
		('Jai Ho (You Are My Destiny)', 'jai ho'),
		('Boys (Summertime Love)', 'boys'),
		('Cheerleader', 'cheerleader'),
		('Hold My Hand', 'hold my hand'),
		('How Deep Is Your Love', 'how deep is your love'),
		('Je sais pas danser', 'je sais pas danser'),
		('Juju On That Beat', 'juju on that'),
		('Jump (For My Love)', 'jump'),
		('Let Me Love You', 'let me love you'),
		('Mambo No. 5 (A Little Bit of Monika)', 'mambo n5'),
		('Mambo No. 5 (A Little Bit of Monika)', 'mambo 5'),
		('Me Too', 'me too'),
		('Mi Gente', 'mi gente'),
		('Shut Up and Dance', 'shut up'),
		('Single Ladies (Put a Ring on It)', 'single ladies'),
		('Smile', 'smile'),
		('Taste The Feeling', 'tase the fee'),
		('The Greatest', 'the greatest'),
		('These Boots Are Made For Walking', 'these boots'),
		('These Boots Are Made For Walking', 'these boots are'),
		('YOUTH', 'youth'),

		(u"Better When I\u2019m Dancin'", 'better when im dancin'),
		(u'Don\u2019t Let Me Down', 'dont le me down'),
		(u'Don\u2019t Worry', 'dont worry'),
		(u'That\u2019s the Way (I Like It)', 'thats the way'),
		(u'Aserej\u00e9 (The Ketchup Song)', 'ketchup'),
		(u'She\u2019s Got Me Dancing', 'she got me dancing'),
		(u'She\u2019s Got Me Dancing', 'shes got me dancing'),
		(u'Sway (Qui\u00e9n Ser\u00e1)', 'sway'),
		(u'Imya 505 (\u0418\u043c\u044f 505)', 'imya'),
		(u'Imya 505 (\u0418\u043c\u044f 505)', 'imya 505'),
		(None, 'utter crap this doesnt exist'),
	]

	q = create_queue()
	for t in tests:
		yield check_match, q, t[0], t[1]

def check_match(q, x, t):
	r = q.match(t)
	assert x == r, 'x={0} <> r={1} [{2}]'.format(x, r, t)
