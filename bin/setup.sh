#!/bin/sh
##
set -eu

PROGBASE=$(d=$(dirname -- "${0}"); cd "${d}" && pwd)

cd "${PROGBASE}/.."
python3 -mvenv venv
. ./venv/bin/activate
exec python3 install -r requirements.txt
