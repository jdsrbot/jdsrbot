#!/bin/sh
##
set -eu

PROGBASE=$(d=$(dirname -- "${0}"); cd "${d}" && pwd)

cd "${PROGBASE}/.."
. ./venv/bin/activate
exec python3 jdsrbot.py
