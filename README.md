# Just Dance song request bot for Twitch

This repository contains the code for a Just Dance song request chat bot
for the Twitch platform. Currently, the bot supports the following
features:

- song requests registration and management (`!sr`);
- song requests printing (`!queue`).

## Usage

- On Windows, execute the `bin/run.bat` script.
- On Linux, execute `bin/run.sh` script.

## Installing

1. Install Python3 and pip.
2. Get the code archive at
   `https://gitlab.com/pgregoire/jdsrbot/-/archive/master/jdsrbot-master.zip`
   or via git using `git clone https://gitlab.com/pgregoire/jdsrbot`.
3. If you're on Windows, unzip the archive and place the `jdsrbot-master`
   directory on your desktop. Rename the `jdsrbot-master` directory to
   `jdsrbot`.
4. Create an app using your Twitch's developer account and fill in the
   `config.py` file at the base of the project's directory. See the below
   section on how to register an app.
5. The installation instructions depend on the platform.
        i. On Windows, execute the `bin/setup.bat` script.
        ii. On Linux, execute the `bin/setup.sh` script.

## Register the app on Twitch

Registering the application gives the developer/user the ability to
use the Twitch's API services and link the application with a user
account. To register `jdsrbot` as an application, first go to the
[Application Dashboard](https://glass.twitch.tv/console/apps/create).
Enter a name for the app and `http://127.0.0.1/` as the OAUTH redirect
URL. Finally, select `Chat Bot` as the application category.

Once the app is register, you will see it displayed on the Application
Dashboard. You will be able to get the app's client id and secret key
by clicking the `Manage` button for the app.
