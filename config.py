
# The channel to monitor and name of the user the bot writes as.
USERNAME = 'your username'

CLIENT_ID = 'the client id of the application'
CLIENT_SECRET = 'the client secret key of the application'

# This must watch what you provided to Twitch when registering
# the application.
REDIRECT_URL = 'http://localhost/'

# See https://twitchapps.com/tokengen/
OAUTH_TOKEN = 'the oauth token associated with the app'

#loglevel = 'debug'

