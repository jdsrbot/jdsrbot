#!/usr/bin/env python
# # -*- coding: utf-8 -*-
#
# Copyright 2018, Philippe Grégoire <pg@pgregoire.xyz>
#
# ==================================================================== #

# TODO extreme

from __future__ import print_function

from fuzzywuzzy import fuzz

import copy
import os
import threading

# -------------------------------------------------------------------- #

class Queue:
	def __init__(self, songs):
		self.songs    = self.clean_songs(songs)
		self.requests = []
		self.lock     = threading.RLock()

		#for s in self.songs: print(s)

	def __request(self, song, user):
		import datetime

		s = self.match(song)
		if not s:
			return s

		for r in self.requests:
			if r['name'] == s:
				for u in r['users']:
					if u == user:
						return s
				r['users'] += [user]
				return s

		n = datetime.datetime.now()
		self.requests.append({'name':s,'users':[user], 'date':n, 'request': song})
		return s
			

	def request(self, song, user):
		"""Make a request."""
		with self.lock:
			s = self.__request(song, user)
			if s:
				self.print()
		return s


	def match(self, song):
		"""Return the closest match to @song."""
		bn = ""
		br = 0

		song = song.lower()

		for s in self.songs:
			n = s['csong']
			if n == s:
				return s['song']

			r  = fuzz.ratio(n, song)
			r += fuzz.partial_ratio(n, song)
			r += fuzz.token_set_ratio(n, song)
			r += fuzz.token_sort_ratio(n, song)
			if r > br:
				br = r
				bn = s['song']


		#import sys
		#print('{0} => {1} at {2}'.format(song, bn, br), file=sys.stderr)

		# TODO ignore if lower than 20%?
		return None if (0.65 * 400) >= br else bn


	def pop(self):
		"""Pop and return the head of the list. None if empty."""
		h = None
		if 0 != len(self.requests):
			h = self.requests[0]
		self.requests = self.requests[1:]

		self.print()
		return h

	def queue(self):
		"""Return the sorted queue."""
		with self.lock:
			m = copy.deepcopy(self.requests)
		return m

	def print(self):
		"""Print the queue on the terminal. (rev order)"""
		os.system('cls' if 'nt' == os.name else 'clear')
		q = self.queue()
		if 0 == len(q):
			print('>> no song requests')
			return

		q.reverse()
		for i in range(0, len(q)):
			_ = q[i]
			m  = '{0}. {1}'.format( (len(q)-i), _['name'])
			m += ' ({0})'.format(', '.join(_['users']))
			m += ' [{0}]'.format(_['request'])
			print(m)

	def clear(self):
		"""Clear the queue."""
		self.requests = []

	def clean_songs(self, songs):
		"""Clean the songs in @songs."""
		r = []
		for s in songs:
			s['csong'] = s['song'].lower()
			r.append(s)

			if 'alt' not in s:
				continue

			for alt in s['alt']:
				c = copy.deepcopy(s)
				c['csong'] = alt.lower()
				r.append(c)
		return r

# ==================================================================== #
