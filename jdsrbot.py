#!/usr/bin/env python
# # -*- coding: utf-8 -*-
#
# Copyright 2018, Philippe Grégoire <pg@pgregoire.xyz>
#
# ==================================================================== #

import config
import http.server
import ircbot
import myqueue
import sys

# -------------------------------------------------------------------- #
#
# localhost interface to issue commands
#

def waitcmd(queue):
	class RequestHandler(http.server.SimpleHTTPRequestHandler):
		def do_GET(self):
			if '/pop' == self.path:
				self.send_response(200)
				queue.pop()
			else:
				self.send_response(404)

			self.send_header("Content-Type", "text/html")
			self.send_header("Content-Length", 0)
			self.end_headers()

		def log_message(self, format, *args):
			return

	a = ('', 8000)
	httpd = http.server.HTTPServer(a, RequestHandler)
	httpd.serve_forever()


def launch(queue):
	import threading

	tok = config.OAUTH_TOKEN
	usr = config.USERNAME
	bot = ircbot.JDSRBot(usr, config.CLIENT_ID, tok, usr, queue)

	class BotThread(threading.Thread):
		def run(self):
			while True:
				try:
					bot.start()
				except Exception as e:
					print(e, file=sys.stderr)
					pass

	class ServerThread(threading.Thread):
		def run(self):
			while True:
				try:
					waitcmd(queue)
				except Exception as e:
					print(e, file=sys.stderr)
					pass

	bt = BotThread()
	bs = ServerThread()
	bt.start()
	bs.start()
	bt.join()
	bs.join()

# -------------------------------------------------------------------- #

if '__main__' == __name__:
	import json

	with open('jdlist.json', 'rb') as fp:
		jd = json.loads(fp.read())

	mgr = myqueue.Queue(jd)
	launch(mgr)

# ==================================================================== #
