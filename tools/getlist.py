#!/usr/bin/env python
# # -*- coding: utf-8 -*-
#
# Copyright 2018, Philippe Grégoire <pg@pgregoire.xyz>
#
# Fetch regular, alternate and mashup songs of Just Dance Unlimited.
#

# normal, alternate, mashups

from lxml import etree
import re
import requests
from io import StringIO

# -------------------------------------------------------------------- #

URL='http://justdance.wikia.com/wiki/Just_Dance_Unlimited'

def parse_row(row):
    # skip the table header
    for _ in row.iter('th'):
        return

    td = []
    for c in row.iter('td'):
        td.append(c)

    if 7 != len(td):
        return

    # song names are links
    song = ""
    for a in td[0].iter('a'):
        if a.text and '' == song:
            song = a.text.encode('utf-8')
    if '' == song:
        # people iz lasy
        for i in td[0].iter('i'):
            if i.text and '' == song:
                song = i.text.encode('utf-8')
    assert('' != song)

    # artist
    artist = td[1].text.strip().encode('utf-8')
    assert('' != artist)

    # year
    year = td[2].text.strip()
    assert('' != year)

    # type
    type = ""
    for b in td[3].iter('b'):
        if '' == type and b.text:
            type = b.text.strip()
    if '' == type:
        if td[3].text:
            type = td[3].text.strip()
    assert('' != type)

    # game
    game = ""
    for a in td[5].iter('a'):
        if a.text and '' == game:
            game = a.text.encode('utf-8')
    if '' == game:
        # people iz lasy
        for i in td[5].iter('i'):
            if i.text and '' == game:
                game = i.text.encode('utf-8')
    # ACCEPTABLE assert('' != game)

    return {
        'song': song,
        'artist': artist,
        'year': year,
        'type': type,
        'game': game
    }

def compile(tree, section, category):
    r = tree.xpath('//*[@id="{0}"]'.format(section))
    i = r[0].getparent()

    while 'table' != i.tag:
        i = i.getnext()

    l = []
    for r in i.iter('tr'):
        s = parse_row(r)
        if s:
            s['category'] = category
            l.append(s)

    return l


def parse(page):
    parser = etree.HTMLParser()
    tree   = etree.parse(StringIO(page), parser)

    ls  = compile(tree, 'Track_List', 'regular')
    ls += compile(tree, 'Alternate_Routines', 'extreme')
    ls += compile(tree, 'Mashups', 'mashups')

    return sorted(ls)

def get():
    import os

    if not os.path.exists('page.html'):
        with open('page.html', 'wb') as fp:
            fp.write(requests.get(URL).text.encode('utf-8'))

    with open('page.html', 'rb') as fp:
        return fp.read().decode('utf-8')

# -------------------------------------------------------------------- #

if '__main__' == __name__:
    import json

    p = parse(get())
    print(json.dumps(p, indent=4, sort_keys=True))

# ==================================================================== #
